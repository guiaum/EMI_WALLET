**EMI_WALLET**

Deux mallettes composées de cubes, capteurs wifi envoyant leurs infos en OSC.
Inspiré de la [Brut Box](https://framagit.org/resonance/brutbox) et développée pour l'[Ensemble de Musique Interactive](http://www.musique-interactive.com/).
Chaque cube embarque une ESP8266, un gestionnaire de charge et une batterie.

![](Pics/DSC08644.JPG)
![](Pics/DSC08649.JPG)

**Mode d'emploi**
1. Allumer le routeur, attendre quelques minutes
2. Connecter l'ordinateur au réseau EMI_WIFI avec le mot de passe EMI_WIFI
Lui affecter l'adresse IP 192.168.31.100, masque 255.255.255.0, passerelle 192.168.31.1
3. Les infos OSC sont envoyées sur le port 9999, les tags sont explicites.
4. Les cubes démarrent automatiquement. 
Lumière bleue : en cours de connection
Lumière verte : connecté, prêt à être utilisé.

Pour la charge: le chargeur indique le niveau de courant utilisé par prise. Ex: [1.0,5] veut dire que la prise USB 1 est en charge. [1.0,0] veut dire que la batterie est chargée. Charger chaque cube lorsqu'il est éteint, stocker les batteries chargées.


**BOM, pour chaque cube, sans les capteurs.**

1. **Rallonge micro-usb** (évite la fragilité des prises des PCB)
[Câble d'Extension Micro USB 2.0 Type B de Mâle vers femelle 15cm](https://www.aliexpress.com/item/15cm-USB-2-0-Type-B-Male-To-Female-M-F-Extension-Charging-Data-Cable-Cord/32952079806.html?spm=a2g0s.13010208.99999999.259.2fa83c00Rn7GKq)
0.79 euro

2. **Gestionnaire de charge**
[CFsunbird Battery Shield V1.1.0 For WEMOS D1 mini](https://www.aliexpress.com/item/CFsunbird-WeMos-D1-Mini-Battery-Shield-Lithium-Battery-Charging-Boost-With-LED-Light-Module-Mini-Micro/32804665725.html?spm=a2g0s.9042311.0.0.9c7b4c4dNa2s0l)
1.72 euro

3. **Batterie** (1000 ou 1200 mah)
[H11C lipo battery 3.7v 1000mah](https://www.aliexpress.com/item/6pcs-H11C-lipo-battery-3-7v-1000mah-JST-batteries-and-charger-with-plug-for-JJRC/1000005562450.html?spm=a2g0s.9042311.0.0.9c7b4c4dNa2s0l)
2.83 euro

4. **ESP8266**
[WEMOS](https://www.aliexpress.com/item/D1-mini-Mini-NodeMcu-4-M-bytes-Lua-ESP8266-WIFI-Internet-das-Coisas-baseado-placa-de/32847899373.html?spm=a2g0s.9042311.0.0.9c7b4c4dNa2s0l)
2.62 euro

5. **[LED RGB](https://www.amazon.fr/gp/product/B00F4MGA0I/ref=oh_aui_detailpage_o04_s00?ie=UTF8&psc=1)**
0.03 euro

6. **3 résistances de 1K**

7. **Reed** (ne pas se fier à l'image, il a bien 3 pattes)
[Farnell](https://fr.farnell.com/webapp/wcs/stores/servlet/ProductDisplay?catalogId=10001&langId=-2&urlRequestType=Base&partNumber=2453628&storeId=10160)
1.88 (achat par 10) euro

8. **Bornier**
[2 pin](https://fr.farnell.com/webapp/wcs/stores/servlet/ProductDisplay?catalogId=10001&langId=-2&urlRequestType=Base&partNumber=2112482&storeId=10160)
0.439

9. 2 vis et 2 écrous carrés de 2.5mm (fixnvis)

10. Aimant (pour éteindre) [0.35 centimes environ](https://www.supermagnete.fr/aimants-disques-neodyme/disque-magnetique-diametre-10mm-hauteur-2mm-neodyme-n42-nickele_S-10-02-N)



**Routeur intégré dans la valise**: [Xiaomi Mi](https://fr.gearbest.com/wireless-routers/pp_620039.html?wid=1433363#goodsDetail) 23 euro
**Chargeur intelligent, indiquant le niveau de charge des batteries**

